# coby-soil-annotation


* **COBY_RELEASE_1.5** :

  * **SourceForge** : https://sourceforge.net/projects/coby-1-5/files/Coby_Releases/coby_bin_1.5.10.zip/download
  * **Sources**     : https://forgemia.inra.fr/anaee-dev/coby.git

#### I - Docker Deployment ( Copy & Paste one of the two following Docker Commands ) : 

#### **I.1 Host Networking :**

```bash 

docker run -d -p 2345:5432 --rm --name db-auth ecoinfo/coby-db-auth ; \
                                                                      \
docker run --net host -d --rm                                         \
           --name coby                                                \
           -e HTTPS_PORT=8585                                         \
           -e DB_URL_AUTHENTICATION="jdbc:postgresql://localhost:2345/coby-db-auth" \
           -e DB_LOGIN_AUTHENTICATION=postgres                                      \
           -e DB_PASSWORD_AUTHENTICATION=admin                                      \
           -v $(pwd)/src/SI/.:/opt/coby/pipeline/SI                                 \
           -v $(pwd)/src/orchestrators/.:/opt/coby/pipeline/orchestrators           \
           -v $(pwd)/infra/coby/.:/opt/coby/jaxy-server/overridden/                 \
           -v $(pwd)/doi_export/:/var/doi-nfs-export/                               \
           ecoinfo/coby:1.6 /opt/coby/jaxy-server/overridden/run_server.sh


```  

#### **I.2 Isolated Network :**
 
```bash

docker run -d -p 2345:5432 --rm --name db-auth ecoinfo/coby-db-auth ; \
                                                                      \
docker run -d --rm                                                    \
           --name coby                                                \
           -p 8181:8181                                               \
           -p 8585:8585                                               \
           -p 7777:7777                                               \
           -p 8888:8888                                               \
           -e HTTPS_PORT=8585                                         \
           -e DB_URL_AUTHENTICATION="jdbc:postgresql://db-auth:5432/coby-db-auth" \
           -e DB_LOGIN_AUTHENTICATION=postgres                                    \
           -e DB_PASSWORD_AUTHENTICATION=admin                                    \
           -v $(pwd)/src/SI/.:/opt/coby/pipeline/SI                               \
           -v $(pwd)/src/orchestrators/.:/opt/coby/pipeline/orchestrators         \
           -v $(pwd)/infra/coby/.:/opt/coby/jaxy-server/overridden/               \
           -v $(pwd)/doi_export/:/var/doi-nfs-export/                             \
           --link db-auth:db-auth                                                 \
           ecoinfo/coby:1.6 /opt/coby/jaxy-server/overridden/run_server.sh
           

```

- ##### 2. Go to URL

```bash

            https://localhost:8585      ( Authentication : admin / admin )
 
```

#### II - Soil Annotation 

![soil-annotation-01](/uploads/c6151d897ed82692626a89b080a2fee2/soil-annotation-01.jpg)


#### III - Sparql Example Queries ( URIs are temporary pending the integration of an ontology ) : 

```bash
    SELECT ?observedProperty  ?methodOfMeasurement  ?type { 

           ?observedProperty <http://localhost/HasMethod> ?methodOfMeasurement .
  
           ?methodOfMeasurement <http://localhost/HasType> ?type .

           FILTER ( str(?type) = "physique") .

    }
```

```bash

    SELECT ?observedProperty  ?methodOfMeasurement  ?type { 

           ?observedProperty <http://localhost/HasMethod> ?methodOfMeasurement .
  
           ?methodOfMeasurement <http://localhost/HasType> ?type .

           FILTER ( str(?type) = "chimique") .

    }
```